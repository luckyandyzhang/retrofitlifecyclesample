package retrofit.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description:
 * Created by Andy on 2017/5/20
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface RetrofitInterface {
}
