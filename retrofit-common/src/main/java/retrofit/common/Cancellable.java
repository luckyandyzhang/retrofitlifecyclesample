package retrofit.common;

import retrofit2.Call;

/**
 * Description:
 * Created by Andy on 2017/5/22
 */

public interface Cancellable {
    void cancelAll(Call... excludes);
}
