package me.andydev.retrofit.lifecycle;

import java.lang.reflect.Constructor;

import retrofit.common.Cancellable;
import retrofit2.Call;


/**
 * Description:
 * Created by Andy on 2017/5/22
 */

public class RetrofitLifecycle {

    public static <T> T getProxyInterface(Class<T> retrofitAPI, T retrofitAPIImpl) {
        String className = retrofitAPI.getName();
        if (className.startsWith("android.") || className.startsWith("java.")) {
            throwsProxyClassNotFoundException(className);
        }

        T proxyClassInstance = null;
        try {
            Class<?> bindingClass = Class.forName(className + "InvokeProxy");
            Constructor<T> constructor = (Constructor<T>) bindingClass.getConstructor(retrofitAPI);
            proxyClassInstance = constructor.newInstance(retrofitAPIImpl);
        } catch (Exception e) {
            e.printStackTrace();
            throwsProxyClassNotFoundException(className);
        }
        return proxyClassInstance;
    }

    public static void cancelAll(Object retrofitAPI, Call... excludes) {
        if (retrofitAPI instanceof Cancellable) {
            ((Cancellable) retrofitAPI).cancelAll(excludes);
        } else {
            throw new IllegalArgumentException(retrofitAPI.getClass().getName() + " must implement Cancellable");
        }
    }

    private static void throwsProxyClassNotFoundException(String className) {
        throw new IllegalArgumentException("Cannot find proxy class for " + className
                + ". Did you forget to add @RetrofitInterface to it?");
    }
}
