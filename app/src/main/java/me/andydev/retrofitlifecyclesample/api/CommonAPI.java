package me.andydev.retrofitlifecyclesample.api;

import me.andydev.retrofitlifecyclesample.bean.GankIO;
import me.andydev.retrofitlifecyclesample.bean.Status;
import me.andydev.retrofitlifecyclesample.bean.UpdateInfo;
import me.andydev.retrofitlifecyclesample.bean.UserInfo;
import retrofit.common.RetrofitInterface;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Description:
 * Created by Andy on 2017/5/20
 */
@RetrofitInterface
public interface CommonAPI {

    int test = 123;

    //检查更新
    @GET("api/v1/latest_assistant")
    Call<UpdateInfo> checkUpdate();

    //请求打开盒子的wifi热点
    @FormUrlEncoded
    @POST("api/v1/wifi")
    Call<Status> openSinanWifi(@Header("Authorization") String token, @Field("sn") String sn);

    //账户信息
    @GET("api/v1/user")
    Call<UserInfo> getUserInfo(@Header("Authorization") String token);

    //test
    @GET("http://gank.io/api/search/query/listview/category/Android/count/10/page/1")
    Call<GankIO> getGankIO();

}
