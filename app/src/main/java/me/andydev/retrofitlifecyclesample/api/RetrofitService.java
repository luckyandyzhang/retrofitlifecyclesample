package me.andydev.retrofitlifecyclesample.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Description:
 * Created by Andy on 2017/5/20
 */

public class RetrofitService {

    private CommonAPI mCommonAPI;
    private TestAPI mTestAPI;

    private Retrofit mRetrofit;

    private RetrofitService() {
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://apis.baidu.com/apistore/")
                    .build();
        }

        if (mCommonAPI == null) {
            mCommonAPI = mRetrofit.create(CommonAPI.class);
        }

        if (mTestAPI == null) {
            mTestAPI = mRetrofit.create(TestAPI.class);
        }

    }

    public static TestAPI getTestAPI() {
        return RetrofitServiceHolder.INSTANCE.mTestAPI;
    }

    public static CommonAPI getCommonAPI() {
        return RetrofitServiceHolder.INSTANCE.mCommonAPI;
    }

    private static class RetrofitServiceHolder {
        private static RetrofitService INSTANCE = new RetrofitService();
    }
}
