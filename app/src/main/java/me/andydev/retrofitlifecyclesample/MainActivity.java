package me.andydev.retrofitlifecyclesample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import me.andydev.retrofit.lifecycle.RetrofitLifecycle;
import me.andydev.retrofitlifecyclesample.api.CommonAPI;
import me.andydev.retrofitlifecyclesample.api.RetrofitService;
import me.andydev.retrofitlifecyclesample.bean.GankIO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private CommonAPI mProxyInterface;
    private Call<GankIO> mCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProxyInterface = RetrofitLifecycle.getProxyInterface(CommonAPI.class,
                RetrofitService.getCommonAPI());

        mCall = mProxyInterface.getGankIO();
        mCall.enqueue(new Callback<GankIO>() {
            @Override
            public void onResponse(Call<GankIO> call, Response<GankIO> response) {
                Log.d(TAG, "onResponse: " + response.body().results.get(0).desc);
            }

            @Override
            public void onFailure(Call<GankIO> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RetrofitLifecycle.cancelAll(mProxyInterface);

        //测试是否已经取消
        if (mCall.isCanceled()) {
            Log.d(TAG, "isCanceled");
        }
    }
}
